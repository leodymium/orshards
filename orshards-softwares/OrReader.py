from src.PythonCLI import Console
from src.basic import basic_commands
from src.orReader import orreader_commands

"""OrReader is the tool used to install OrSHARdS packages.
"""
if __name__=='__main__':
    console = Console()

    console.addCommands(basic_commands)
    console.addCommands(orreader_commands)

    console.run()
