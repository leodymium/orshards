from src.PythonCLI import Command
import os
import time
import json

PACKAGE_STORAGE = "./PackageGeneration"

class Package():
    def __init__(self):
        self.package_path = None
        self.project_name = None
        self.project_authors = None
        self.project_version = 0.1
        self.dependencies_list = None
        self.description = None

    def set_project_name(self, name):
        self.project_name = name

    def get_project_name(self):
        return self.project_name

    def set_project_authors(self, names):
        self.set_project_authors = names

    def get_project_authors(self):
        return self.set_project_authors

    def set_description(self, description):
        self.description = description

    def set_version(self, version):
        self.project_version = version

    def set_dependencies_list(self, dependencies):
        self.dependencies_list = dependencies

    def description_file_generation(self, fp):
        description = {"description":self.description}
        json.dump(control, fp, ensure_ascii=False, indent=2)


    def control_file_generation(self, fp):
        control = {
            "Package":self.project_name,
            "Maintainers":self.project_authors,
            "Version":self.project_version,
            "Date": time.strftime("%d/%m/%Y - %H:%M", time.localtime()),
            "Dependencies":self.dependencies_list
        }

        json.dump(control, fp, ensure_ascii=False, indent=2)

    def generate_package(self):
        """This function create all the package file.
        """
        try:
            os.mkdir(PACKAGE_STORAGE)
        except FileExistsError:
            pass

        try:
            os.mkdir(os.path.join(PACKAGE_STORAGE, self.project_name.replace(" ", "")))
        except FileExistsError:
            pass

        self.package_path = os.path.join(PACKAGE_STORAGE, self.project_name.replace(" ", ""))

        try:
            os.mkdir(os.path.join(self.package_path, 'orshards'))
        except:
            pass

        with open(os.path.join(self.package_path, 'orshards', "DESCRIPTION.json"), 'w') as fp:
            self.description_file_generation(fp)

        with open(os.path.join(self.package_path, 'orshards', "CONTROL.json"), 'w') as fp:
            self.control_file_generation(fp)

    def check_package_consistency(self, path):
        pass # TODO


    def load_package(self, path):
        # TODO : a try-catch that can raise a specific exception in path is not a package
        self.check_package_consistency(path)

        self.package_path = path

        with open(os.path.join(self.package_path, 'orshards', 'CONTROL.json'), 'r') as fp:
            control = json.load(fp)
            self.project_name = control["Package"]
            self.project_authors = control["Maintainers"]
            self.project_version = control["Version"]
            self.dependencies_list = control["Dependencies"]

        with open(os.path.join(self.package_path, 'orshards', 'DESCRIPTION.json'), 'r') as fp:
            description = json.load(fp)
            self.description = description["description"]

    def __str__(self):
        output = f"Package : {self.project_name} v{self.project_version}\n"
        output+= "Authors :"

        for author in self.project_authors:
            output+= f"{author}  "
        output+="\n"
        output+= "Dependencies : "
        for dependency in self.dependencies_list:
            output+= f"{dependency}  "
        output+="\n"
        output+=f"Project description :\n{self.description}"
        return output

class PackCommand(Command):
    def __init__(self, name):
        super().__init__(name)
        self.package = Package()

    def new_package(self):
        print("Welcome to the OrSHARdS package creation. We will guide you through the creation of an OrSHARdS package.")
        print("Every information can be modified.")

        # print(self.package)
        """Basic informations set up"""
        # Project name
        print("First, name you project and get started :")
        project_name = input() or None
        if project_name == None:
            print("Project creation aborted.")
            return None
        self.package.set_project_name(project_name)

        # Project authors :
        print("Then, enter project authors :")
        project_authors = input().split(sep=",") or ""
        self.package.set_project_authors(project_authors)

        # Project description
        print("Please enter a description of your project")
        project_description = input() or ""
        self.package.set_description(project_description)

        # Project version
        print("Please enter project version")
        project_version = input() or 0.1
        self.package.set_version(project_version)

        self.package.generate_package()


    def execution(self, params):
        assert len(params["args"]) > 0  , "Pack command takes 1 parameters"

        if params["args"][0] == "new":
            self.new_package()
            return None

        elif params["args"][0] == "load":
            self.package_path = os.path.join(PACKAGE_STORAGE, params["args"][1])
            self.package.load_package(self.package_path)

        elif params["args"][0] == "current":
            if self.package.package_path != None:
                print(f"Current package : {self.package.get_project_name()}")
            else:
                print("No package loaded.")

        

package_commands = [("pack", PackCommand("pack"))]
