from src.PythonCLI import Command

import os
import json

"""A netlist is an object that references the bound between a hardware project folder and
its OrSHARdS version.
"""

class Netlist():
    """The netlist object reference the link beetwin an existing project and the standard of an OrSHARdS package
    """
    def __init__(self, source_path, dest_path):
        """The standard netlist is read from a JSON file located in source_path, and the new netlist is stored in dest_path

        :param String source_path: the path where the standard netlist is stored.
        :param String dest_path: the path where the user-generated netlist is to be stored.
        """
        self.source_path = source_path
        self.dest_path = dest_path

        with open(self.source_path, 'r') as fp:
            self.structure = json.load(fp)

        self.header = self.structure["header"]
        self.body = self.structure["body"]

    def display_netlist(self):
        """Display the netlist.
        """
        for key in self.header:
            print(key, self.header[key])

        for key in self.body:
            print(key, self.body[key])

    def edit_netlist(self, key):
        """Allow the user to modify an entry.

        :param string key: The key of the entry to be modified.
        """
        if key in self.header:
            self.header[key] = input(key+" ("+self.header[key]+") : ")

        if key in self.body:
            if type(self.body[key]) == list:
                print(key)
                while True:
                    user_input = input(" > ")

                    if len(user_input) == 0:
                        break

                    self.body[key].append(user_input)
            else:
                self.body[key] = input(key+" ("+self.body[key]+") : ")

    def fill_netlist(self):
        """Asks the user for the whole information to fill a new netlist.
        """
        for key in self.header:
            self.edit_netlist(key)
        for key in self.body:
            self.edit_netlist(key)

    def generate_netlist(self):
        """Create the file that contains the user netlist.
        """
        fp = open(self.dest_path, 'w')

        json.dump({"header":self.header, "body":self.body}, fp, ensure_ascii=False, indent=2)

class NetlistCommand(Command):
    """Netlist command.

    $ netlist new|load|display|edit [<args>]
    """
    def execution(self, params):
        if params["args"][0] == "new":
            """Generate a new netlist based on the standard netlist structure
            """
            self.netlist = Netlist("./src/standard_netlist.txt", os.path.join(os.getcwd(), params["args"][1]))
            self.netlist.fill_netlist()
            self.netlist.generate_netlist()

        elif params["args"][0] == "load":
            """Load an existing netlist.
            """
            self.netlist = Netlist(params["args"][1], params["args"][1])

        elif params["args"][0] == "display":
            """Display the current netlist.
            """
            self.netlist.display_netlist()

        elif params["args"][0] == "edit":
            """Edit one entry of the netlist.
            """
            self.netlist.edit_netlist(params["args"][1])
            self.netlist.generate_netlist()

netlist_commands = [("netlist", NetlistCommand("netlist"))]

if __name__ == "__main__":
    netlist = Netlist("standard_netlist.txt", "")

    netlist.display_netlist()

    netlist.fill_netlist()

    netlist.display_netlist()
