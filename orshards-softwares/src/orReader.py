import os
import json
import time
from zipfile import ZipFile
from src.guides import Step, Guide, Manual
from src.package import Package
from src.PythonCLI import Command

"""This files contains all the commands and classes used to install, control and manage
an OrSHARdS package.
"""


class InstallCommand(Command):
    def __init__(self, name):
        super().__init__(name)

        self.zipped_package_path = None
        self.package_path = os.path.join('.', 'InstalledPackages')

    def execution(self, params):
        assert len(params["args"]) == 0, "install command takes 0 argument"

        self.zipped_package_path = input("Zipped package path :")
        self.package_path = input(
            f"Package installation directory : (Default : {self.package_path})") or self.package_path

        print(self.zipped_package_path)
        print(self.package_path)

        try:
            os.mkdir(self.package_path)
        except FileExistsError:
            pass

        control = None
        with ZipFile(self.zipped_package_path, 'r') as zipObj:
            for name in zipObj.namelist():
                print(name)
                if 'orshards/CONTROL.json' in name:
                    zipObj.extractall(self.package_path)
                    with open(os.path.join(self.package_path, name), 'r') as fp:
                        control = json.load(fp)

            if control == None:
                print("No orshards file found.")
                raise AssertionError

        # TODO : change that so the path contains package_name
        # with open(os.path.join(self.package_path, 'orshards', 'CHANGELOG.txt'), 'w') as fp:
        #     local_time = time.strftime("%d/%m/%Y - %H:%M", time.localtime())
        #     fp.write(f"[{local_time}] Downloading package\n")
        #


class ManagePackage(Command):
    def __init__(self, name):
        super().__init__(name)

        self.package = Package()
        self.package_path = None
        self.manual_path = None
        self.manual = None

    def load_package(self, path):
        self.package = self.package.load_package(path)

    def execution(self, params):
        assert len(params["args"]) > 0, "more than 1 parameter required"
        if params["args"][0] == "load":
            self.package_path = input("Enter package path :")
            self.package.load_package(self.package_path)

        elif params["args"][0] == "current":

            if self.package.package_path != None:
                print(f"Current package : {self.package.get_project_name()}")
            else:
                print("No package loaded.")
        elif params["args"][0] == "info":
            print(str(self.package))
        elif params["args"][0] == "install":
            self.manual_path = os.path.join(
                self.package.package_path, 'usr/share/man')
            self.manual = Manual.load_manual(self.manual_path)

            for guide in self.manual.guides:
                for step in guide.steps:
                    print(str(step))
                    input("Press enter to complete step")
                    step.state = 1
                    self.manual.dump_manual(self.manual_path)


orreader_commands = [("install", InstallCommand("install")),
                     ("pack", ManagePackage("pack"))]
