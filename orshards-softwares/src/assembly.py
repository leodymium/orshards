from src.PythonCLI import Command
import os

"""Deprecated assembly instruction generator.
"""

class Guide():
    def __init__(self):
        self.guide_path = ""
        self.current_part = ""
        self.current_step = ""

    def is_empty(self):
        return self.guide_path == ""

    def __str__(self):
        return f"Path: {self.guide_path}\nPart: {self.current_part}\nStep: {self.current_step}"

class GuideCommands(Command):
    def __init__(self, name):
        super().__init__(name)
        self.guide = Guide()

    def execution(self, params):
        if params["args"][0] == "current":
            if self.guide.is_empty():
                print("No guide loaded. Use \'guide load <guide path>\' to load a guide.")
            else:
                print("Current Guide :\n", self.guide)


        elif params["args"][0] == "add":
            if self.guide.is_empty():
                print("No guide loaded. Use \'guide load <guide path>\' to load a guide.")
                return None

            title = ' '.join(params["args"][1:])

            if params["flags"][0] == "part":
                part_path = os.path.join(self.guide.guide_path, title + '.md')
                self.guide.current_part = part_path


                # Creating a new file for the part in the guide
                fp = open(part_path, 'w')

                fp.write('# ' + title + "\n")
                img_path = input("You can add an image to represent this part") or "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"

                fp.write(f"<img src=\"{img_path}\" alt=\"{title}\" width=\"200\"/>\n")
                fp.write("\n\n")
                fp.close()

                # Adding the part to the landing page

                fp  = open(os.path.join(self.guide.guide_path, "landing.md"), 'a')
                fp.write("* [.]("+title+".md){step}\n")
                fp.close()

            elif params["flags"][0] == "step":
                fp = open(self.guide.current_part , 'a+')

                fp.write('## ' + title + "\n")
                img_path = input("You can add an image to represent this step") or "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"

                fp.write(f"<img src=\"{img_path}\" alt=\"{title}\" width=\"200\"/>\n")
                fp.write("\n")

                fp.write(input("Write the step instructions :\n"))

                fp.write("\n")

                fp.close()

        elif params["args"][0] == "new":
            folder_name = params["args"][1]

            # First, a assembly_manuals is generated if it does not exists for the moment
            try:
                os.mkdir('./assembly_manuals')
            except FileExistsError:
                pass

            # Then, a subfolder for the guide
            try:
                folder_path = os.path.join('./assembly_manuals', folder_name)
                os.mkdir(folder_path)
                self.guide.guide_path = folder_path

                os.mkdir(os.path.join(folder_path, 'images'))

            except FileExistsError:
                raise GuideExistsError

            print("You are starting a new guide")
            print("Please enter a few informations to get started !")

            config_file = open(os.path.join(folder_path, 'buildconf.yaml'), 'w')

            # Project name
            print("Project Name :")
            project_title = input()
            config_file.write("Title: " + project_title)
            config_file.write("\n")

            # Authors :
            config_file.write("LandingPage: landing.md\n\nAuthors:\n")
            print("Author(s) : separate with a coma if there are several authors")
            authors_list = input().split(sep=', ')

            for author in authors_list:
                config_file.write("    - " + author + "\n")

            config_file.write("\n")

            # Affiliation
            print("Affiliation :")
            affiliation = input()
            config_file.write("Affiliation: " + affiliation)

            config_file.write("\n")

            # Contact
            print("Email :")
            affiliation = input()
            config_file.write("Email: " + affiliation)
            config_file.write("\n")

            config_file.close()
            # Setting the landing page

            landing = open(os.path.join(folder_path, "landing.md"), 'w')
            landing.write(f"# {project_title}\n\n")

            desc = input(
                "You may want to add a short description of the project : ") or ""

            landing.write(desc+"\n\n")

            landing.close()

        elif params["args"][0] == "load":
            print(params["args"][1])
            if os.path.isdir(params["args"][1]):
                self.guide.guide_path = params["args"][1]
                print("Loaded guide with path", self.guide.guide_path)
            else:
                print("Guide not found")


        else:
            print("Unknown arguments :", params["args"][0])

assembly_commands = [("guide", GuideCommands("guide"))]
