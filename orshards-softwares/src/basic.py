from src.PythonCLI import Command
import sys
import os

"""Basic commands for a CLI program.
"""

class Exit(Command):
    """A command to exit the program.
    $ exit
    """

    def execution(self, parsed_inputs):
        sys.exit(0)


class Ls(Command):
    """A command to list the content of a directory
    $ ls [<path>+]
    """

    def execution(self, parsed_inputs):
        """ls command

        :param dict parsed_inputs: dictionnary containing 'command', 'args', and 'flag' keys.
        :return A list of directory within the path passed in 'args'
        """

        output = ""
        dir_list = []

        # If no argument is provided, the current directory is displayed
        if len(parsed_inputs["args"]) == 0:
            dir_list = os.listdir(".")
            for file in dir_list:
                output += file + "  "

        # If only one path is provided, the directory is displayed
        elif len(parsed_inputs["args"]) == 1:
            try:
                dir_list = os.listdir(parsed_inputs["args"][0])
            except:
                output = "ls: cannot access '" + \
                    parsed_inputs["args"][0] + "': No such file or directory"

            for file in dir_list:
                output += file + "  "

        # TODO : for more than one path, display every content.

        return output


class Cd(Command):
    """A command to change the current directory
    $ cd [<path>]
    """
    def execution(self, parsed_inputs):
        """cd command
        Change the directory to a specific directory. If no path is provided, change for '~'

        :param dict parsed_inputs: dictionnary containing 'command', 'args', and 'flag' keys.
        :return None
        """

        if len(parsed_inputs["args"]) == 0:
            os.chdir(os.path.expanduser("~"))
        elif len(parsed_inputs["args"]) == 1:
            try:
                os.chdir(parsed_inputs["args"][0])
            except FileNotFoundError:
                print("Unable to find path", parsed_inputs["args"][0])
                raise FileNotFoundError


class Help(Command):
    def __init__(self, name):
        super().__init__(name)
        with open("Readme.md") as fp:
            self.help = fp.read()

    def execution(self, parsed_inputs):
        if len(parsed_inputs["args"]) == 0:
            print(self.help)


exitCommand = Exit("exit")
lsCommand = Ls("ls")
cdCommand = Cd("cd")
helpCommand = Help("help")

basic_commands = [("exit", exitCommand), ("ls", lsCommand),
                  ("cd", cdCommand), ("help", helpCommand)]
