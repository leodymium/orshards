from src.PythonCLI import Command
from src.package import Package
import os
import pickle
"""This is an alternative to the assembly.py way to create guides.

- How does a manual work ?
A manual is unique for a specific package.
It contains many guides (They can be for different version of the project,
or different big steps), and a guide can contains some steps.

Each step as a difficulty, and contains instructions, infos, images and document
relative to the state.

- TO DO :
A step should be complete if every test and control are passed.
A guide is completed if every step is completed.

- Command list :

man new : Create a new manual for a package
man load : load the manual of a package to allow modifications
man guide add : Create a new guide in a manual
man guide load : Load a guide from a manual
man guide current : Display the current guide you are working on.
man step add : Create a new step for the current manual.
man display : Display the current manual
man save : Save the modifications
"""

class Step():
    def __init__(self, name):
        self.name = name
        self.state = 0
        self.difficulty = 0
        self.instructions = []
        self.documents = []
        self.images = []

        self.infos = []
        self.test = None
        self.control = None

    def __str__(self):
        output = f"{self.name}\n"
        output += f"Difficulty : {self.difficulty}\n"
        for instruction in self.instructions:
            output += f"* {instruction}\n"
        output+="\n"

        for info in self.infos:
            output += f"i - {info}\n"
        output+="\n"

        output+="Step images : \n"
        for image in self.images:
            output += f"* {image}\n"
        output+="\n"

        output+="Step documents : \n"
        for doc in self.documents:
            output += f"* {doc}\n"
        output+="\n"

        return output

class Guide():
    def __init__(self, name):
        self.name = name
        self.state = 0
        self.steps = []

    def add_step(self, step):
        """Add a step to the guide.
        """
        if isinstance(step, Step):
            self.steps.append(step)
        else:
            print("Error adding step")

    def __str__(self):
        output = self.name+"\n"
        for i, step in enumerate(self.steps):
            output+=f"    {i+1}. {str(step.name)}\n"
        return output

class Manual():
    def __init__(self, name):
        self.state = 0
        self.name = name
        self.guides = []

    def add_guide(self, guide):
        """Add a guide to the package manual.
        """
        if isinstance(guide, Guide):
            self.guides.append(guide)
        else:
            print("Error adding guide")

    def dump_manual(self, path):
        """Save the manual in the assembly file of the package.
        """
        with open(os.path.join(path, "assembly"), 'wb') as fp:
            pickle.dump(self, fp)

    def load_manual(path):
        """Load the manual of a package from the assembly file.
        """
        with open(os.path.join(path, "assembly"), 'rb') as fp:
            manual = pickle.load(fp)
        return manual

    def __str__(self):
        output = self.name+":\n"
        for i, guide in enumerate(self.guides):
            output+=f"  {i+1}. {str(guide)}\n"

        return output


class GuideCommands(Command):
    def __init__(self, name):
        super().__init__(name)
        self.package_path = None
        self.package = None
        self.manual = None
        self.manual_path = None

        self.current_guide = None
        self.current_step = None

    def execution(self, params):
        if params["args"][0] == "new":
            print("Please enter the path of the package")
            self.package_path = input() or ""

            if not (os.path.isdir(os.path.join(self.package_path, "orshards"))) :
                print("The specified package is not an OrSHARdS package.") # DEBUG
                return None

            self.package = Package()
            self.package.load_package(self.package_path)

            self.manual_path = os.path.join(self.package_path, 'usr/share/man')
            try:
                os.makedirs(self.manual_path)
            except FileExistsError:
                pass
            except FileNotFoundError:
                 print(f"Unable to create specific folder '{self.manual_path}'")
                 return None

            self.manual = Manual(self.package.get_project_name())

            print(f"{self.manual.name} manual correcly created. Start now by creating a first guide with the command `man guide add`.")

        elif params["args"][0] == "load":
            print("Please enter the path of the package")
            self.package_path = input() or ""

            if not (os.path.isdir(os.path.join(self.package_path, "orshards"))) :
                print("The specified package is not an OrSHARdS package.") # DEBUG
                return None

            self.package = Package()
            self.package.load_package(self.package_path)

            self.manual_path = os.path.join(self.package_path, 'usr/share/man')
            self.manual = Manual.load_manual(self.manual_path)

            print(f"{self.manual.name} manual correcly loaded.")

        elif params["args"][0] == "guide":
            if params["args"][1] == "add":
                print("Enter guide's name :")
                guide_name = input() or "Unnamed"

                self.current_guide = Guide(guide_name)
                self.manual.add_guide(self.current_guide)
            elif params["args"][1] == "current":
                print("Current guide :", self.current_guide)
            elif params["args"][1] == "load":
                print("Input the number of the guide you want to load")
                num = input() or ""
                if num != "":
                    self.current_guide = self.manual.guides[int(num) - 1]

        elif params["args"][0] == "step":
            if params["args"][1] == "add":
                print("Enter step name : ")
                step_name = input() or ""

                self.current_step = Step(step_name)
                self.current_guide.add_step(self.current_step)

                print("You can now add step instructions. To stop, just add an empty instruction.")

                instruction = ""
                while 1:
                    instruction = input("Text : ") or ""

                    if instruction == "":
                        break

                    self.current_step.instructions.append(instruction)

                print(self.package_path)
                print(f"You can add images to the step. Images must be stored in {os.path.join(self.package_path, 'img')}. To stop, just press enter without text.")
                image_path = ""
                while 1:
                    image_path = input(f"Image path : {os.path.join(self.package_path, 'img')}") or ""

                    if image_path == "":
                        break

                    self.current_step.images.append(image_path)

                print("You may want to add extra informations. To stop, just press enter without text.")
                info = ""
                while 1:
                    info = input("Info : ") or ""

                    if info == "":
                        break

                    self.current_step.infos.append(info)

                print("Add CAD files. To stop, just press enter without text.")
                doc = ""
                while 1:
                    doc = input("Doc : ") or ""
                    if doc == "":
                        break
                    self.current_step.documents.append(doc)



                print("Please add test : FOR THE MOMENT NO TEST")# TODO : Developp the testing system

                print("Please evaluate step difficulty (1 to 10)")
                self.current_step.difficulty = int(input()) or 1

            if params["args"][1] == "current":
                print(str(self.current_step))

        elif params["args"][0] == "display":
            if self.manual == None:
                return "No manual as been loaded. Please use 'man new' or 'man load' to create or to load a manual."
            else:
                return str(self.manual)
        elif params["args"][0] == "save":
            print(self.manual_path)
            self.manual.dump_manual(self.manual_path)


guide_commands = [("man", GuideCommands("man"))]
