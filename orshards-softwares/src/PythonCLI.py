import os
import sys
import re


class Console():
    def __init__(self, name='PythonCLI'):
        self.name = name
        self.init_path = os.getcwd()
        self.command_dictionnary = {}

    def parseInputs(self, user_inputs):
        """This function parse a standard user input (string) into a dictionnary.

        Ex : user_inputs = "command_name --flag1 --flag2 arg1 arg2 --flag3"
        -> input = {
            "command":command_name,
            "flags":[flag1, flag2, flag3],
            "args":[arg1, arg2]
        }
        """
        user_inputs = user_inputs.split(sep=' ')
        input = {}

        input["command"] = user_inputs[0]
        input["flags"] = []
        input["args"] = []

        for user_entry in user_inputs[1:]:
            if user_entry[:2] == '--':
                input["flags"].append(user_entry[2:])
            else:
                input["args"].append(user_entry)

        return input

    def addCommand(self, command_name, command_object):
        """Add a new command to the console command_dictionnary. The command will be called by the user using command name.

        :param str command_name: The alias the user will use to call the command.
        :param Command command_object: The Command object that contains the instructions.
        """
        self.command_dictionnary[command_name] = command_object

    def addCommands(self, command_list):
        """Add many commands at a time.

        :param list command_list: [(command_name, command_object), ("command1", Command1), ("command2", Command2)]
        """
        for command in command_list:
            try:
                self.command_dictionnary[command[0]] = command[1]
            except:
                print("Error adding command", command)

    def execCommand(self, parsed_inputs):
        """Execute a command if it exists in the command_dictionnary.

        :param dict parsed_inputs: User inputs parsed with the parseInputs method.
        Print the command output if not None.
        """
        command_output = None
        try:
            command_output = self.command_dictionnary[parsed_inputs["command"]].execution(parsed_inputs)
        except KeyError:
            print("Unknow command :", parsed_inputs["command"])
        except AssertionError:
            print("Error in command parameters")
        if command_output != None:
            print(command_output)

    def run(self):
        """Program main loop. Waits for user input, then parse and try to execute them.
        """
        while(True):
            user_inputs = input("# " + os.getcwd() + " : ")

            parsed_inputs = self.parseInputs(user_inputs)

            self.execCommand(parsed_inputs)


class Command():
    """Abstract class for a command.
    """
    def __init__(self, name):
        self.name = name

    def help(self):
        pass

    def execution(self, params):
        pass
