from src.PythonCLI import Console
from src.basic import basic_commands
from src.netlist import netlist_commands
from src.assembly import assembly_commands
from src.package import package_commands
from src.guides import guide_commands

"""main.py is the file where the console object for the package creation part is instanciated. Then, we add
the needed commands with addCommands method, and start the program with run()
method.
"""
if __name__ == '__main__':
    console = Console()

    console.addCommands(basic_commands)
    console.addCommands(netlist_commands)
    # console.addCommands(assembly_commands)
    console.addCommands(guide_commands)
    console.addCommands(package_commands)

    console.run()
