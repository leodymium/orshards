# Welcome to the OrSHARdS project !

# OrSHARdS  

The OrSHARdS project aims to suggest a standard relative to the dissemination of Open Source Hardware.  

The software can be use to create a new hardware project, or to generate an OrSHARdS package from an existing project. You can also use it to install package in your house or lab, and to manage the lifecycle of existing package.  

Find more informations about ORSHARDS here : https://gitlab.com/leodymium/orshards

For now, OrSHARdS software only exists as a command line tool. If you want to contribute, don't hesitate to check the wiki.  

You can find package to test the tool in the Package repository.  

# How to use ?  

To create an OrSHARdS package, you just need to run the ProjetCreator tool by running `python3 ProjectCreator.py`.  
Then, you can use commands associate to the Package Creation Tool and follow the instructions.  

To install an OrSHARdS package, you may use the OrReader tool by running `python3 OrReader.py`. THen, you can use commands associate to the OrReader tool and follow the instructions.  

# Commands

Standard commands :  

* `cd [<path='~'>]`           : change the current directory to 'path'.  

* `help`                      : display the help on how to use the program.  

* `ls [<path='.'>]`           : list the content of the folder 'path'.  

* `exit`                      : Exit the program.  

## Package Creation Tool  

Assembly guide generation :  
First version, deprecated :  
* `guide new <guide_name>`    : Generate a new assembly guide.  

* `guide current`             : Display information about the current guide.  

* `guide add <--part|--step> <title>`  : Add a new part or step to current guide.  

* `guide load <path>`         : Change the current guide to the guide in <path>.  

New version :  
* `man new`                   : Create a new manual for a package.

* `man load`                  : load the manual of a package to allow modifications.

* `man guide add`             : Create a new guide in a manual.

* `man guide load`            : Load a guide from a manual.

* `man guide current`         : Display the current guide you are working on.

* `man step add`              : Create a new step for the current manual.

* `man display`               : Display the current manual.

* `man save`                  : Save the modifications.

Package creation :  

* `pack new`                  : Create a new OrSHARdS package.  

* `pack load`                 : Load an OrSHARdS package.  

* `pack current`              : Display the name of the currently loaded package.  

Netlist :  

* `netlist new <netlist_name>` : Create a new netlist to convert an already existing project into an OrSHARdS package.  

* `netlist load <path>`       : Load a netlist.  

* `netlist display`           : Display the loaded netlist.  

* `netlist edit <key>`        : Edit the specified key.

## OrReader  

Package Installation :  

* `install`                   : Tool to install on a computer an OrSHARdS package from a Zip Archive.  

* `pack load`                 : Load an installed package.  

* `pack current`              : Display the current loaded package.  

* `pack info`                 : Display current package informations.  

* `pack install`              : Run the assembly instructions for the loaded package.  


# What to do now ?  

This repository is only a first shot of what the OrSHARdS project aims to be.  The purpose is to develop enough functionnality to offer a first tool to control and manage Hardware dissemination.  
It will be necessary to clarify the code, and to write something more structured. The actual structure is full of default, and should only be used as a Proof Of Concept.  
